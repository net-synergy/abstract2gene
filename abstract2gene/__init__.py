from . import data, genes, model, nlp

__all__ = ["nlp", "genes", "model", "data"]
